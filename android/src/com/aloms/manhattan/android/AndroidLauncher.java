package com.aloms.manhattan.android;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.aloms.manhattan.GdxGame;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;



public class AndroidLauncher extends AndroidApplication  {
	
	 private static final String AD_UNIT_ID_BANNER = "ca-app-pub-4182579003074950/1940128026";
	 private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-4182579003074950/5201318827";
    private final int SHOW_ADS = 1;
    private final int HIDE_ADS = 0;


    AdView adView;
    RelativeLayout layout;

	   @Override public void onCreate (Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);

           // Create the layout
           layout = new RelativeLayout(this);

           // Do the stuff that initialize() would do for you
           requestWindowFeature(Window.FEATURE_NO_TITLE);
           getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                   WindowManager.LayoutParams.FLAG_FULLSCREEN);
           getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
           getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);


           IActivityRequest IActivity = new IActivityRequest();
           IActivity.add(handler);

           // Create the libgdx View
           View gameView = initializeForView( new GdxGame( IActivity   ) );
           layout.addView(gameView);


           AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

           // Optionally populate the ad request builder.

           adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
           adRequestBuilder.addTestDevice("D487AAE7969921F7B5FB3341FA5883FE");

           // Create and setup the AdMob view
           adView = new AdView(this); // Put in your secret key here
           adView.setAdSize(AdSize.BANNER);
           adView.setAdUnitId(AD_UNIT_ID_BANNER);

           adView.setEnabled(true);
           adView.resume();
           adView.bringToFront();
           adView.loadAd( adRequestBuilder.build() );

           adView.setVisibility(View.GONE);
           adView.setAdListener(new AdListener() {
               private boolean _first = true;

               @Override
               public void onAdLoaded() {
                   if (_first) {
                       _first = false;

                       // Add the AdMob view when the first ad gets loaded, this makes sure the first ad gets displayed
                       RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                       adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM); // places the ad at the bottom overlapping your game
                       adParams.addRule(RelativeLayout.CENTER_IN_PARENT);

                       adView.setVisibility(View.VISIBLE);
                       layout.addView(adView, adParams);
                   }
               }
           });


           // Hook it all up
           setContentView(layout);




	    }

    @Override
    public void onPause() {
        adView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    public void onDestroy() {
        adView.destroy();
        super.onDestroy();
    }

    protected Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case SHOW_ADS: {
                    adView.setVisibility(View.VISIBLE);
                    break;
                }
                case HIDE_ADS: {
                    adView.setVisibility(View.GONE);
                    break;
                }
            }
        }
    };


}
