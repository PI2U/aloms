package com.aloms.manhattan.android;

import android.os.Handler;

import com.aloms.manhattan.IActivityRequestHandler;


/**
 * Created by Patrick on 25.01.2015.
 */

public class IActivityRequest implements IActivityRequestHandler {

    private final int SHOW_ADS = 1;
    private final int HIDE_ADS = 0;

    Handler handler;
    private boolean dummy = false;

    public void add(Object h){

       if(h == null){
           dummy = true;
       }else{
           handler = (Handler) h;
       }

    }

    public void showAds(boolean show) {

        if(!dummy) {
            handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
        }
    }

}
