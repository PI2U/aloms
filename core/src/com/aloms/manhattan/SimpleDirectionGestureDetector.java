package com.aloms.manhattan;

import com.badlogic.gdx.input.GestureDetector;

/**
 * Created by Patrick on 28.01.2015.
 */
public class SimpleDirectionGestureDetector extends GestureDetector {
    public SimpleDirectionGestureDetector(DirectionListener directionListener) {
        super(new DirectionGestureListener(directionListener));
    }

    public interface DirectionListener {


        void onZoom(float distance, float initialDistance);

        void onFling(float velocityX, float velocityY);

        void onPan(float deltaX,float deltaY);
    }

    private static class DirectionGestureListener extends GestureAdapter {
        DirectionListener directionListener;

        public DirectionGestureListener(DirectionListener directionListener) {
            this.directionListener = directionListener;
        }

        @Override
        public boolean touchDown (float x, float y, int pointer, int button) {
            return false;
        }

        @Override
        public boolean tap (float x, float y, int count, int button) {
              return false;
        }

        @Override
        public boolean zoom(float initialDistance, float distance) {



            directionListener.onZoom(distance , initialDistance);
            return false;
        }

        @Override
        public boolean fling(float velocityX, float velocityY, int button) {



            directionListener.onFling(velocityX,velocityY);


            return super.fling(velocityX, velocityY, button);
        }

        @Override
        public boolean pan (float x, float y, float deltaX, float deltaY) {


            directionListener.onPan(deltaX, deltaY);

            return false;
        }


    }

}