package com.aloms.manhattan;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Patrick on 06.02.2015.
 */
public class Weapon {

    public int damage = 100,weapon_speed;
    public Rectangle weapon_rect;
    public Texture weapon_texture;
    public boolean render_weapon = false;



    public Weapon(int type){

        weapon_rect = new Rectangle();
        weapon_rect.x = 0;
        weapon_rect.y = GdxGame.player_center.y;
        weapon_rect.width = Gdx.graphics.getWidth() / 10;
        weapon_rect.height = weapon_rect.width;


        damage = 100;

        switch (type){
            case 1:
                weapon_texture = new Texture("doener.png");
                weapon_speed = 185;
            case 2:
                weapon_texture = new Texture("doener.png");
                weapon_speed = 385;
            default:
                weapon_texture = new Texture("doener.png");
                weapon_speed = 585;

        }


        weapon_speed = GdxGame.height / weapon_speed;


    }

    public void update(Kunde kunde){

        if(render_weapon){
            weapon_rect.y += weapon_speed;
        }

        if (render_weapon && kunde.overlaps(weapon_rect)  ) {
           render_weapon = false;
           kunde.hit(damage);



        } else if (render_weapon && weapon_rect.y >= GdxGame.height * 0.85) {
           render_weapon = false;
        }

    }

    public void render(SpriteBatch batch){

        if(render_weapon){
            batch.draw(weapon_texture,weapon_rect.x,weapon_rect.y,weapon_rect.width,weapon_rect.height);

        }
    }


    public void change(int type){

    }


    public void fire() {

        render_weapon = true;
        weapon_rect.setCenter(GdxGame.player_center.x, GdxGame.player_center.y);
    }
}
