package com.aloms.manhattan;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.LinkedList;
import java.util.Random;


public class GdxGame extends ApplicationAdapter {
    public static boolean menue = true, credits = false, karte = false;
    public static float zoom = 1.0f;

    final float INITIAL_SPEED = 0.15f;
    final int INITIAL_LEBEN = 10;
    final float SPEED_INCREASE = 0.065f;
    final float KUNDE_HORIZONTAL_SPEED = 0.1f;


    SpriteBatch batch, fontBatch,creditBatch;
    Texture floorImage, bucketImage, menu, karte1, karte2,gameoverImage;
    Texture btn_start, btn_exit, btn_credits, btn_karte, btn_menu, btn_retry, credits_bg;
    Sound moneySound, gameOverSound, failSound;
    Music mainMusic;
    OrthographicCamera camera;
    Rectangle bucket,  btn_start_rect, btn_exit_rect, btn_credits_rect, btn_karte_rect,btn_menu_rect,btn_retry_rect;
    BitmapFont font;
    Vector2 karte_pos;
    public static Vector2 player_center;
    Kunde kunde[][];
    int width;
    int level=0;
    static int height;
    static int punkte;
    int leben = INITIAL_LEBEN;
    int highscore;
    float speed = INITIAL_SPEED, degree, degree_kunde, touchX, touchY;
    long time_old, time_old_kunde;
    boolean  reset_done = false, gameover = false,gameover_sound= false;
    Preferences prefs;
    LinkedList<String> music_files;
    Music.OnCompletionListener next_music_listener;
    Random rnd = new Random();
    Weapon weapon;

    Animation doenerSpiessAnimation;
    Texture                         doenerspiessTexture;
    TextureRegion[]                 doenerspiessFrames;
    TextureRegion                   doenerspiessCurrentFrame,doenerspiessCurrentFrame2;

    float stateTime;





    private IActivityRequestHandler myRequestHandler;


    public GdxGame(IActivityRequestHandler handler) {
        myRequestHandler = handler;
    }

    public GdxGame() {

    }



    @Override
    public void dispose() {
        batch.dispose();
        fontBatch.dispose();
        creditBatch.dispose();
    }

    @Override
    public void create() {


        prefs = Gdx.app.getPreferences("Preferences");

        highscore = prefs.getInteger("highscore", 0);

        width = com.badlogic.gdx.Gdx.graphics.getWidth();
        height = com.badlogic.gdx.Gdx.graphics.getHeight();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, width, height);

        batch = new SpriteBatch();
        fontBatch = new SpriteBatch();
        creditBatch = new SpriteBatch();

        floorImage = new Texture("floor.png");
        bucketImage = new Texture("bedienung.png");

        menu = new Texture("menu.jpg");
        btn_start = new Texture("btn_start.png");
//        btn_start_active = new Texture("btn_start_active.png");
        btn_exit = new Texture("btn_exit.png");
//        btn_exit_active = new Texture("btn_exit_active.png");
        btn_credits = new Texture("btn_credits.png");
        btn_menu = new Texture("btn_menu.png");
        btn_retry = new Texture("btn_retry.png");

        credits_bg = new Texture("credits.jpg");
//        btn_credits_active = new Texture("btn_credits_active.png");
        btn_karte = new Texture("btn_karte.png");
        karte1 = new Texture("karte1.jpg");
        karte2 = new Texture("karte2.jpg");
        gameoverImage = new Texture("gameover.jpg");
        doenerspiessTexture = new Texture("doenerspiess.png");


        TextureRegion[][] tmp = TextureRegion.split(doenerspiessTexture, doenerspiessTexture.getWidth()/2, doenerspiessTexture.getHeight()/2);
        doenerspiessFrames = new TextureRegion[4];
        int index = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                doenerspiessFrames[index++] = tmp[i][j];

            }
        }
        doenerSpiessAnimation = new Animation(0.30f, doenerspiessFrames);

        stateTime = 0f;

        karte_pos = new Vector2(0.0f,0.0f);

        player_center = new Vector2();

        kunde = new Kunde[4][4];

        for (int x = 0; x < 4; x++) {

            for (int y = 0; y < 4; y++) {

                kunde[x][y] = new Kunde(1, width / 7 + (x * width / 5), (int) (height - height * 0.2) - (y * height / 12), com.badlogic.gdx.Gdx.graphics.getWidth() / 10, com.badlogic.gdx.Gdx.graphics.getHeight() / 12);

            }


        }


        kunde[1][2].changeType(1);
        kunde[2][2].changeType(2);

        font = new BitmapFont(Gdx.files.internal("calibri.fnt"), Gdx.files.internal("calibri.png"), false);

        // load the drop sound effect and the rain background "music"


        gameOverSound = Gdx.audio.newSound(Gdx.files.internal("sound/game_over.mp3"));
        failSound = Gdx.audio.newSound(Gdx.files.internal("sound/fail1.mp3"));


        music_files = new LinkedList<String>(  );

        for( FileHandle e: Gdx.files.internal("music/").list() ){

            music_files.add( e.path() );
        }


        mainMusic = Gdx.audio.newMusic(Gdx.files.internal( music_files.getFirst() ) );
        mainMusic.setVolume(0.00f);

        // start the playback of the background music immediately

        next_music_listener = new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {

                    //mainMusic = Gdx.audio.newMusic(Gdx.files.internal(music_files.get(rnd.nextInt(music_files.size()))));

                    mainMusic = Gdx.audio.newMusic(Gdx.files.internal("music/000menu.mp3"));
                    mainMusic.setVolume(0.50f);
                    mainMusic.setOnCompletionListener(next_music_listener);
                    mainMusic.play();



            }
        };


        //mainMusic.setOnCompletionListener(next_music_listener);

        mainMusic.setVolume(0.50f);
        mainMusic.setLooping(true);
        mainMusic.play();

        bucket = new Rectangle();
        bucket.x = width / 2 - 64 / 2;
        bucket.y = (float) (height - (height * 0.96));
        bucket.width = width / 10;
        bucket.height = height / 10;



        btn_start_rect = new Rectangle();
        btn_exit_rect = new Rectangle();
        btn_credits_rect = new Rectangle();
        btn_karte_rect = new Rectangle();
        btn_menu_rect = new Rectangle();
        btn_retry_rect = new Rectangle();





        btn_start_rect.setSize((width / 2.2f), height / 8);
        btn_exit_rect.setSize(btn_start_rect.getWidth(), btn_start_rect.getHeight());
        btn_credits_rect.setSize(btn_start_rect.getWidth(), btn_start_rect.getHeight());
        btn_karte_rect.setSize(btn_start_rect.getWidth(), btn_start_rect.getHeight());
        btn_menu_rect.setSize(btn_start_rect.getWidth(), btn_start_rect.getHeight());
        btn_retry_rect.setSize(btn_start_rect.getWidth(), btn_start_rect.getHeight());


        btn_start_rect.setCenter( width / 2, height / 1.75f );
        btn_exit_rect.setCenter( width / 2, btn_start_rect.y - btn_start_rect.height/1.75f );
        btn_credits_rect.setCenter( width / 2, btn_exit_rect.y - btn_exit_rect.height/1.75f );
        btn_karte_rect.setCenter( width / 2, btn_credits_rect.y - btn_credits_rect.height/1.75f );
        btn_menu_rect.setCenter( width / 2, height/2 );
        btn_retry_rect.setCenter( width / 2.0f , height/2-btn_menu_rect.height-btn_start_rect.height/1.75f);

        weapon = new Weapon(2);

        InputMultiplexer mux = new InputMultiplexer();

        mux.addProcessor(new MyInputProcessor());


        mux.addProcessor(new SimpleDirectionGestureDetector(new SimpleDirectionGestureDetector.DirectionListener() {



            @Override
            public void onFling(float velocityX, float velocityY){


                //System.out.println("Fling " + velocityX + " , " + velocityY);

            }

            @Override
            public void onPan(float deltaX, float deltaY) {
                //System.out.println("PAN:  deltaX: " + deltaX + " deltaY: " + deltaY);
                karte_pos.x += deltaX;
                karte_pos.y -= deltaY;
            }

            @Override
            public void onZoom(float distance, float initialDistance) {

                float amount = distance/initialDistance;
                //System.out.println("initialDistance: " + initialDistance + "  distance: " + distance + " amount: " + amount);

                if (amount > 1) {
                    zoom -= (0.03f * amount);
                } else {
                    zoom += (0.015f * amount);
                }

                if (zoom < 0.3f) {
                    zoom = 0.3f;
                } else if (zoom > 1.5f) {
                    zoom = 1.5f;
                }

            }

        }));



        font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        font.scale(0.4f);

        Gdx.input.setInputProcessor(mux);
        Gdx.input.setCatchBackKey(true);

    }

    public boolean contains(Rectangle rect, float x, float y) {

        if (y >= rect.y && y <= rect.y + rect.height && x >= rect.x && x <= rect.x + rect.width) {

            return true;
        }

        return false;
    }

    public void reset(){

        Kunde.alive_cnt = 0;
        leben = INITIAL_LEBEN;
        speed = INITIAL_SPEED;
        punkte = 0;
        gameover = false;
        level = 0;
        zoom = 1.0f;
        camera.zoom = 1.0f;
        camera.update();
        mainMusic.play();
    }



    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stateTime += Gdx.graphics.getDeltaTime();
        doenerspiessCurrentFrame = doenerSpiessAnimation.getKeyFrame(stateTime, true);
        doenerspiessCurrentFrame2 = doenerSpiessAnimation.getKeyFrame(stateTime-0.25f, true);


        if (menue) {
            if(!reset_done){
                reset();
                reset_done = true;

            }
            handleMenu();
            return;
        } else if (credits) {

            handleCredits();
            return;
        } else if (karte) {

            handleKarte();
            return;
        }else if(!menue && reset_done){
            reset_done = false;
        }

        mainLoop();


    }

    private void mainLoop() {

        bucket.x = (float) ((Math.sin(degree)) + 1) * width / 2.27f;

        if (Gdx.input.isTouched()) {
            bucket.getCenter(player_center);
            weapon.fire();
        }


        if(!gameover) {
            if (TimeUtils.millis() - time_old >= 30) {
                degree += KUNDE_HORIZONTAL_SPEED;

                degree_kunde = (float) ((Math.sin(-degree)) + 1) * width / 3.5f - Kunde.kunde_dimension.x / 4;
                time_old = TimeUtils.millis();
            }

        }

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(floorImage, 0, 0, width, height);
        batch.draw(doenerspiessCurrentFrame, width-doenerspiessCurrentFrame.getRegionWidth(), 0);
        batch.draw(doenerspiessCurrentFrame, width-doenerspiessCurrentFrame.getRegionWidth()*2.1f, 0);
        batch.draw(bucketImage, bucket.x, bucket.y, bucket.width, bucket.height);


        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {

                kunde[x][y].setXPos(degree_kunde + x * Kunde.kunde_dimension.x * 1.3f);

                kunde[x][y].kunde.y -= speed;

                weapon.update(kunde[x][y]);

                if (kunde[x][y].alive && kunde[x][y].kunde.getY() <= (float) (height - (height * 0.85))) {


                    failSound.play(1.0f, -rnd.nextFloat(), -rnd.nextFloat());
                    leben--;
                    kunde[x][y].kill();
                }
                kunde[x][y].draw(batch);
            }
        }





        if(gameover){



            if(gameover_sound) {
                if (myRequestHandler != null) {
                    myRequestHandler.showAds(true);
                }
                mainMusic.pause();
                gameOverSound.play();
                gameover_sound = false;
            }

            speed = 0;

            batch.draw(gameoverImage, 0, 0, width, height);
            batch.draw(btn_retry, btn_retry_rect.x, btn_retry_rect.y, btn_retry_rect.width, btn_retry_rect.height);
            batch.draw(btn_menu, btn_menu_rect.x, btn_menu_rect.y, btn_menu_rect.width, btn_menu_rect.height);

            if (Gdx.input.isTouched()) {

                touchX = Gdx.input.getX();
                touchY = height - Gdx.input.getY();


                if (contains(btn_menu_rect, touchX, touchY)) {

                    //System.out.println("MENU");
                    menue = true;

                } else if (contains(btn_retry_rect, touchX, touchY)) {

                    //System.out.println("RETRY");
                    if (myRequestHandler != null) {
                        myRequestHandler.showAds(false);
                    }
                    reset();
                }

                try{
                    Thread.sleep(80);
                }catch(Exception e){

                }

            }

        }

        weapon.render(batch);
        batch.end();

        fontBatch.begin();


        font.draw(fontBatch, "Lives " + leben, 0 , height);
        font.draw(fontBatch, "Level " + level, width / 2.2f, height);

        font.draw(fontBatch, "Score " + Integer.toString(punkte), 0, height-font.getCapHeight()*1.3f );
        font.draw(fontBatch, "Highscore " + highscore, width / 2.2f, height-font.getCapHeight()*1.3f );


        fontBatch.end();

        if (Kunde.alive_cnt <= 0) {

            for (int x = 0; x < 4; x++) {
                for (int y = 0; y < 4; y++) {

                    kunde[x][y].kunde.x = width / 7 + (x * width / 5);
                    kunde[x][y].kunde.y = (float) ((height - height * 0.2) - (y * height / 12));
                    kunde[x][y].alive();

                }
            }
            speed += SPEED_INCREASE;
            level++;
        }
        if(leben <= 0){

            if(punkte > highscore){
                highscore = punkte;
                prefs.putInteger("highscore", highscore);
                prefs.flush();
            }
            leben = 1337;
            gameover = true;
            gameover_sound = true;

        }

    }


    public void handleKarte() {
        if (myRequestHandler != null) {
            myRequestHandler.showAds(false);
        }

        camera.zoom = zoom;
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(karte1, karte_pos.x, karte_pos.y, karte1.getWidth(), height);
        batch.draw(karte2, karte_pos.x+karte1.getWidth(), karte_pos.y, karte2.getWidth(), height);
        batch.end();
    }

    float color_r = 0.0f,color_g=1.0f,color_b=0.0f;

    public void handleCredits() {
        if (myRequestHandler != null) {
            myRequestHandler.showAds(false);
        }

        creditBatch.setProjectionMatrix(camera.combined);
        creditBatch.begin();


        creditBatch.draw(credits_bg,0,0,width,height);



        if(TimeUtils.millis()-time_old >= 700){

            color_r = rnd.nextFloat();
            color_g = rnd.nextFloat();
            color_b = rnd.nextFloat();

            color_r += 0.30f;
            color_g += 0.30f;
            color_b += 0.30f;

            if(color_r >= 1.0f){
                color_r = 0.99f;
            }
            if(color_g >= 1.0f){
                color_g = 0.99f;
            }
            if(color_b >= 1.0f){
                color_b = 0.99f;
            }
            time_old = TimeUtils.millis();
        }

        creditBatch.setColor( color_r,color_g,color_b, 1.0f );

        creditBatch.end();

    }


    public void handleMenu() {
        karte = credits = false;
        if (myRequestHandler != null) {
            myRequestHandler.showAds(true);
        }
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(menu, 0, 0, width, height);
        batch.draw(btn_start, btn_start_rect.x, btn_start_rect.y, btn_start_rect.width, btn_start_rect.height);
        batch.draw(btn_karte, btn_karte_rect.x, btn_karte_rect.y, btn_karte_rect.width, btn_karte_rect.height);
        batch.draw(btn_exit, btn_exit_rect.x, btn_exit_rect.y, btn_exit_rect.width, btn_exit_rect.height);
        batch.draw(btn_credits, btn_credits_rect.x, btn_credits_rect.y, btn_credits_rect.width, btn_credits_rect.height);

        batch.end();


        if (Gdx.input.isTouched() && Gdx.input.justTouched()) {

            touchX = Gdx.input.getX();
            touchY = height - Gdx.input.getY();

            try {
                Thread.sleep(100);
            } catch (Exception e) {

            }
            if (contains(btn_start_rect, touchX, touchY)) {

                //Gdx.app.log("Start pressed", "TouchX " + touchX + " TouchY " + touchY);

                if (myRequestHandler != null) {
                    myRequestHandler.showAds(false);
                }

                menue = false;

            } else if (contains(btn_exit_rect, touchX, touchY)) {

                //Gdx.app.log("Exit pressed", "TouchX " + touchX + " TouchY " + touchY);
                Gdx.app.exit();


            } else if (contains(btn_credits_rect, touchX, touchY)) {

                //Gdx.app.log("Credits pressed", "TouchX " + touchX + " TouchY " + touchY);
                credits = true;
                menue = false;

            } else if (contains(btn_karte_rect, touchX, touchY)) {

                //Gdx.app.log("Karte pressed", "TouchX " + touchX + " TouchY " + touchY);
                karte = true;
                menue = false;

            }
        }
    }


}
