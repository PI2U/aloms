package com.aloms.manhattan;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by Patrick on 28.01.2015.
 */
public class MyInputProcessor implements InputProcessor {


    public float initialScale = 1.0f;

    @Override
    public boolean keyDown(int keycode) {

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {

        if (keycode == 4 && GdxGame.menue == true) {
            Gdx.app.exit();
        } else if (keycode == 4) {
            GdxGame.menue = true;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
       // System.out.println("touchDown()" + x + " " + y + " " + pointer + " " + button);
        initialScale = GdxGame.zoom;


        return false;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
       // System.out.println("touchUp()" + x + " " + y + " " + pointer + " " + button);

        return false;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {


        //System.out.println("touchDragged()" + dragged );

        return false;
    }

    @Override
    public boolean mouseMoved(int x, int y) {
       // System.out.println("mouseMoved()" + x + " " + y);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {

        //System.out.println("scrolled()" + amount);

        //Zoom out
        //if (amount > 0 && GdxGame.zoom < 1) {
        //    GdxGame.zoom += 0.1f;
        //}

        //Zoom in
        //if (amount < 0 && GdxGame.zoom > 0.1) {
        //    GdxGame.zoom -= 0.1f;
        //}

        return true;
    }


}
