package com.aloms.manhattan;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


public class Kunde {

    public static int alive_cnt = 0;
    public Texture kundeImage[];
    public Vector2 pos;
    public Rectangle kunde;
    public boolean alive = true;
    public int initial_live = 100;
    public int live = initial_live;
    public int type = 0;
    public static Sound killSound;
    public static Sound hitSound;

    public static Vector2 kunde_dimension;

    public Kunde(int type, int posx, int posy, int width, int height) {


        kundeImage = new Texture[3];

        kundeImage[0] = new Texture("kunde0.png");
        kundeImage[1] = new Texture("kunde1.png");
        kundeImage[2] = new Texture("kunde2.png");

        kunde = new Rectangle();

        kunde.x = posx;
        kunde.y = posy;
        kunde.width = width;
        kunde.height = height;

        if(killSound == null){
            killSound = Gdx.audio.newSound(Gdx.files.internal("sound/money.mp3"));
        }

        if(hitSound == null){
            hitSound = Gdx.audio.newSound(Gdx.files.internal("sound/fail1.mp3"));
        }

        if (kunde_dimension == null) {
            kunde_dimension = new Vector2(kunde.width, kunde.height);
        }

        alive_cnt++;
    }

    public void draw(SpriteBatch batch) {
        if (alive) {
            batch.draw(kundeImage[type], kunde.x, kunde.y, kunde.width, kunde.height);
        }
    }

    public boolean hit(int damage){

        live -= damage;

        if(live<=0){
            kill();
            killSound.play();
            return true;
        }

        hitSound.play();
        return false;
    }

    public void kill() {

        alive = false;
        GdxGame.punkte++;
        alive_cnt--;
    }

    public void alive() {
        live = initial_live;
        alive = true;
        alive_cnt++;
    }

    public boolean overlaps(Rectangle doener) {

        if (alive) {
            return doener.overlaps(kunde);
        }

        return false;
    }

    public void setXPos(float degree_kunde) {
        kunde.x = degree_kunde;

    }


    public void changeType(int type) {
        this.type = type;
        initial_live = initial_live + initial_live * type;
        live = initial_live;
    }

}
